#include <stdio.h>
typedef void action_t();
typedef void (*action_ptr_t)();

void print_greeting();
void call_twice();
void call_3_times(void (*func()));
void call_4_times(action_t* func);
void call_5_times(action_ptr_t func);


int main()
{
    call_twice(print_greeting);
    call_3_times(&print_greeting);
    call_4_times(&print_greeting);
    call_5_times(&print_greeting);
    return 0;
}

void print_greeting()
{
   printf("Hello user\n\r");
   fflush(stdout);
}

void call_twice(void func())
{
    func();
    func();
}

void call_3_times(void (*func()))
{
    func();
    func();
    func();
}

void call_4_times(action_t* func)
{
    for(int i=0; i<4; i++)
    {
        func();
    }
}

void call_5_times(action_ptr_t func)
{
    for(int i=0; i<5; i++)
    {
        func();
    }
}

