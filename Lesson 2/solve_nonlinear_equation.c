/*******************************************************************************
* Title                 :   Solve given nonlinear equation using numerical 
                            Netwon's method.
* Filename              :   solve_nonlinear_equation.c
* Author                :   Piotr Zieliński
* Origin Date           :   12/09/2022
* Compiler              :   gcc (MinGW)
*******************************************************************************/
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

typedef double (*equation_t)(const double);

typedef enum
{
    STOP_MAX_ITERATIONS, // Maximum number of iterations reached
    STOP_EPS             // Required precision of calculations reached
} solver_stop_reason_t;

typedef struct solver_result_t
{
    unsigned int iterations;    // Number of iterations used to achieve required precision
    double solution;       // Solution achieved by meeting the required precision or max iterations
    solver_stop_reason_t stop_reason; // Stop reason
} solver_result_t;

typedef struct solver_init_t
{
    solver_result_t* res;           // Ptr to struct storing results
    unsigned int max_iterations;    // Maximum number of iterations
    double eps;                // Precision at which the algorithm stops iterations
    equation_t equation_to_solve;   // Pointer to equation
    equation_t derivative_of_equation;  // Pointer to derivative of the equation

    /* Initial solution for starting iteration. 
    Vallues further from actual solution will increase needed iterations.*/
    double x_initial;   
} solver_init_t;

double equation_to_solve(const double x)
{
    return ((double)powl(x, 2.0L) - 4.0L);
}

double derivative_of_equation(const double x)
{
    return 2*x;
}

double solve_nonlinear_equation(solver_init_t * const init)
{
    double x = init->x_initial; // Solution that will change with every iteration.
    double fx;         // Value of the function for solution x
    double fdx;        // Value of derivative of the function for solution x
    solver_result_t * const res = init->res;
    res->iterations = 0;
    bool precision_reached = false, max_itr_reached = false;

    while (true)
    {
        /* Calculations */
        fx  = init->equation_to_solve(x);
        fdx = init->derivative_of_equation(x);
        x = x - fx / fdx;
        res->iterations++;

        /* Checks if can continue iterating */
        precision_reached = fx <  init->eps && fx > -init->eps;
        if (precision_reached)
        {
            res->stop_reason = STOP_EPS;
            break;
        }
        max_itr_reached = res->iterations >= init->max_iterations;
        if (max_itr_reached)
        {
            res->stop_reason = STOP_MAX_ITERATIONS;
            break;
        }
    }    
    return x;
}

int main()
{
    solver_result_t sol_res;

    solver_init_t sol_init = {
        .x_initial = 1.0L,
        .eps = 1e-12,
        .max_iterations = 10000U,
        .equation_to_solve = &equation_to_solve,
        .derivative_of_equation = &derivative_of_equation,
        .res = &sol_res
    };    
    
    double x_result = (double)solve_nonlinear_equation(&sol_init);

    printf("Solution of given equation for initial value of x_init = %lf"
            " is x = %lf. \n\r", (double)sol_init.x_initial, x_result);
    return 0;
}