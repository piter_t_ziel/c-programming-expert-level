#include <stdlib.h>
#include <stdio.h>

/**
 * @brief Compare two integers - sorting in ascending order
 * 
 * @param a Pointer to first integer
 * @param b Pointer to second integer
 * @return int Result of comparison.
 *              >0 when a > b
 *              <0 when a < b
 *              0 when a==b
 */
int compare_ints_asc(const void *a, const void *b)
{
    const int a_int = *(const int*)a;
    const int b_int = *(const int*)b;

    return a_int - b_int;
}

/**
 * @brief Compare two integers - sorting in descending order
 * 
 * @param a Pointer to first integer
 * @param b Pointer to second integer
 * @return int Result of comparison.
 *              >0 when a < b
 *              <0 when a > b
 *              0 when a==b
 */
int compare_ints_dsc(const void *a, const void *b)
{
    const int a_int = *(const int*)a;
    const int b_int = *(const int*)b;

    return b_int - a_int;
}

int main()
{
    int data[] = {4, 6, 4, 5, 1, 9, 821, -1234};
    const size_t len = sizeof(data) / sizeof(int);
    const size_t size_of_type = sizeof(int);

    printf("Input data:\n\r");
    for (size_t i = 0; i < len-1; i++)
    {
        printf("%d, ", data[i]);
    }
    printf("%d\n", data[len-1]);
    
    /* ------| Ascending order sorting | ---------------------- */
    qsort(data, len, size_of_type, &compare_ints_asc);

    printf("Data sorted in ascending order:\n\r");
    for (size_t i = 0; i < len-1; i++)
    {
        printf("%d, ", data[i]);
    }
    printf("%d\n", data[len-1]);
    
    /* ------| Ascending order sorting | ---------------------- */
    qsort(data, len, size_of_type, &compare_ints_dsc);

    printf("Data sorted in descending order:\n\r");
    for (size_t i = 0; i < len-1; i++)
    {
        printf("%d, ", data[i]);
    }
    printf("%d\n", data[len-1]);

    return 0;
}