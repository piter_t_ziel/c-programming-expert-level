
<#
  .SYNOPSIS
  Recursively removes all .exe files from script location. Prompts about item removal.

  .DESCRIPTION
  Recursively removes all .exe files from script location. Prompts about item removal.

  .INPUTS
  None. You cannot pipe objects to Remove-All-Execs.ps1.

  .OUTPUTS
  None. Remove-All-Execs.ps1 does not generate any output.

  .EXAMPLE
  PS> .\Remove-All-Execs.ps1
#>


function Get-ScriptDirectory {
    Split-Path -Parent $PSCommandPath
}

Write-Host ""
Write-Host "This script will remove all compiled .exe files from this workspace and its subdirectories."
$workspace_location = Get-ScriptDirectory
$execs = Get-ChildItem -Recurse -Path $workspace_location -Filter *.exe

if ($null -eq $execs)
{
    Write-Host "No execs found."
    Exit 1
}
Write-Host "Found following files:"

foreach ($file_name in $execs)
{
    Write-Host $file_name
}

Remove-Item -Path $execs -Confirm
Exit 0

