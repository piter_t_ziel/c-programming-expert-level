# C Programming - Expert Level Course

[[_TOC_]]

## About this course

This is an 3rd part of courses about C programming from InterTech Academy.

Links:

- [Udemy version](https://www.udemy.com/course/jezyk-c-poziom-ekspert)
- [InterTech Academy store version](https://kursy.intertechacademy.pl/product/jezyk-c-poziom-ekspert/)
- [Repository with code from InterTech Academy instructors](https://github.com/mackrasz/ekspertc)

## Topics

[Lesson 1 - Introduction](/Lesson%201/)
[Lesson 2 - Function pointer](/Lesson%202/)

## Used software

[MinGW](https://osdn.net/projects/mingw/)

[Visual Studio Code](https://code.visualstudio.com/)
